<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta name="HandheldFriendly" content="True" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="format-detection" content="address=no" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, initial-scale=1.0, shrink-to-fit=no">

    <title><?=$generalInfo['title'] ?? 'Cvetaeva'?></title>

    <meta property="og:type" content="website">
    <meta property="og:title" content="<?=$generalInfo['title'] ?? 'Cvetaeva'?>">
    <meta property="og:description" content="<?=$generalInfo['description'] ?? 'Cvetaeva'?>">
    <meta name="description" content="<?=$generalInfo['description'] ?? 'Cvetaeva'?>">
    <meta property="og:url" content="<?=HOST?>">
    <meta property="og:image" content="images/<?=$generalInfo['seo_logo']?>">

    <!-- styles -->
    <link rel="stylesheet" type="text/css" href="../css/slick.css" />
    <link rel="stylesheet" type="text/css" href="../css/slick-theme.css" />
    <link rel="stylesheet" type="text/css" href="../css/jquery.fancybox.css" />
    <link rel="stylesheet" href="../css/front.css?v=2.3">
    <!--[if lt IE 9]> <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script> <![endif]-->
</head>

<body>
<header class="header">
    <div class="header-inner">
        <div class="logo">
            <img src="images/<?=$generalInfo['logo']?>" alt="<?=$generalInfo['title'] ?? 'Cvetaeva'?>">
        </div>
        <nav class="main-menu-wrap">
            <ul class="main-menu">
                <li><a href="#about-section">ОБО МНЕ</a>
                </li>
                <li><a href="#portfolio-section">ГАЛЕРЕЯ</a>
                </li>
                <li><a href="#review-section">ОТЗЫВЫ</a>
                </li>
                <li><a href="#footer">КОНТАКТЫ</a>
                </li>
            </ul>
        </nav>

        <div class="mnu-btn">
            <span></span>
            <span></span>
            <span></span>
        </div>

        <div class="header-contacts">
            <a href="tel:<?=$phoneWithoutSpaces?>" class="contact-phone"><?=$generalInfo['phone']?></a>
<!--            <div class="socials">-->
<!--                <a href="--><?//=$generalInfo['link_facebook'] ?? '#'?><!--" class="socials-item socials-fb"></a>-->
<!--                <a href="--><?//=$generalInfo['link_insta'] ?? '#'?><!--" class="socials-item socials-in"></a>-->
<!--                <a href="--><?//=$generalInfo['link_vk'] ?? '#'?><!--" class="socials-item socials-vk"></a>-->
<!--            </div>-->
        </div>
    </div>
</header>

<section class="main-section" style="background-image: url(../images/<?=$sectionMain['image']?>)">
    <div class="main-section-inner">
        <div class="main-banner">
            <div class="main-banner-text">
                <?=$sectionMain['text']?>
            </div>
            <a data-fancybox data-src="#hidden-content" href="javascript:;" class="btn">Отвечу на все вопросы</a>
        </div>
    </div>
</section>

<section class="about-section" id="about-section">
    <div class="about-wrap">
        <div class="about-item">
            <div class="about-item-left" style="background-image: url(../images/<?=$sectionAboutMe['image']?>)"></div>
            <div class="about-item-right" style="background-image: url(../images/<?=$sectionAboutMe['image_desc']?>)">
                <div class="about-item-content">
                    <?=$sectionAboutMe['text']?>
                </div>
            </div>
        </div>
        <div class="about-item">
            <div class="about-item-left" style="background-image: url(../images/<?=$sectionNoname1['image']?>)"></div>
            <div class="about-item-right" style="background-image: url(../images/<?=$sectionNoname1['image_desc']?>)">
                <div class="about-item-content">
                    <?=$sectionNoname1['text']?>
                </div>
            </div>
        </div>
        <div class="about-item">
            <div class="about-item-left" style="background-image: url(../images/<?=$sectionNoname2['image']?>)"></div>
            <div class="about-item-right" style="background-image: url(../images/<?=$sectionNoname2['image_desc']?>)">
                <div class="about-item-content">
                    <?=$sectionNoname2['text']?>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="video-section" style="background-image: url(../images/videobg.png)">
    <div class="container">
        <div class="section-head">
            <?=$sectionVideo['description']?>
        </div>
    </div>
    <div class="video-outer">
        <div class="video-inner">
            <video controls="controls" poster="../images/<?=$sectionVideo['poster']?>>">
                <source src="../images/<?=$sectionVideo['video']?>">
            </video>
        </div>
    </div>
</section>

<section class="portfolio-section" id="portfolio-section">
    <div class="container">
        <div class="section-head">
            <?=$sectionPortfolio['description']?>
        </div>
    </div>
    <div class="portfolio-wrap">
        <div class="portfolio-left">
            <div class="portfolio-item">
                <img src="../images/<?=$sectionPortfolio['image_1']?>" alt="portfolio">
            </div>
            <div class="portfolio-row">
                <div class="portfolio-item">
                    <img src="../images/<?=$sectionPortfolio['image_4']?>" alt="portfolio">
                </div>
                <div class="portfolio-item">
                    <img src="../images/<?=$sectionPortfolio['image_5']?>" alt="portfolio">
                </div>
            </div>
        </div>
        <div class="portfolio-right">
            <div class="portfolio-row">
                <div class="portfolio-item">
                    <img src="../images/<?=$sectionPortfolio['image_2']?>" alt="portfolio">
                </div>
            </div>
            <div class="portfolio-row">
                <div class="portfolio-item">
                    <img src="../images/<?=$sectionPortfolio['image_3']?>" alt="portfolio">
                </div>
                <div class="portfolio-item">
                    <img src="../images/<?=$sectionPortfolio['image_6']?>" alt="portfolio">
                </div>
            </div>
        </div>
    </div>
</section>

<section id="review-section" class="review-section" style="background: #0c0f16 url(../images/<?=$sectionReviews['image'] ?? ''?>) no-repeat; background-size:cover">
    <div class="container">
        <div class="section-head"><?=$sectionReviews['description']?></div>
        <div class="review-slider-wrap">
            <div class="review-slider">
                <?php foreach ($reviewsRow as $reviews): ?>
                    <div class="review-slider-item">
                        <div class="review-wrap">
                            <?php foreach ($reviews as $review): ?>
                                <div class="review-item">
                                    <?=$review['text'] ?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>

<footer id="footer" class="footer" style="background-image: url(../images/<?=$sectionContacts['image']?>)">
    <div class="container">
        <div class="footer-inner">
            <div class="footer-content">
                <?=$sectionContacts['text']?>
                <a href="tel:<?=$phoneWithoutSpaces?>" class="contact-phone"><?=$generalInfo['phone']?></a>
                <div class="socials socials-footer">
                    <a href="<?=$generalInfo['link_facebook'] ?? '#'?>" target="_blank" class="socials-item socials-fb" rel="nofollow"></a>
                    <a href="<?=$generalInfo['link_insta'] ?? '#'?>" target="_blank" class="socials-item socials-in" rel="nofollow"></a>
                    <a href="<?=$generalInfo['link_youtube'] ?? '#'?>" target="_blank" class="socials-item socials-yb" rel="nofollow"></a>
                </div>
                <a href="mailto:<?=$generalInfo['email']?>" class="mail"><?=$generalInfo['email']?></a>
                <a data-fancybox data-src="#hidden-content" href="javascript:;" class="btn">Обратный звонок</a>
            </div>
        </div>
    </div>
</footer>

<div class="modal" style="display: none;" id="hidden-content">
    <div class="modal-inner">
        <div class="modal-bg" style="background-image: url(../images/modal.jpg)"></div>
        <div class="modal-content">
            <h3>ПОЛУЧИТЬ КОНСУЛЬТАЦИЮ</h3>
            <form action="">
                <label for="iname">ВАШЕ ИМЯ</label>
                <input type="text" placeholder="Введдите ваше имя" id="iname" class="m-input">
                <label for="itel">ВАШ ТЕЛЕФОН</label>
                <input type="text" placeholder="Введдите ваш номер телефона" id="itel" class="m-input">
                <input type="submit" value="Отправить" class="m-submit">
            </form>
        </div>
    </div>
</div>


<script src="../js/libs.js"></script>
<script src="../js/common.js?v=1.2"></script>
</body>

</html>
<?php

$generalInfo = $database->find('general_info');
$sectionMain = $database->find('section_main');
$sectionAboutMe = $database->find('section_about_me');
$sectionNoname1 = $database->find('section_noname_1');
$sectionNoname2 = $database->find('section_noname_2');
$sectionVideo = $database->find('section_video');
$sectionPortfolio = $database->find('section_portfolio');
$sectionReviews = $database->find('section_reviews');
$sectionContacts = $database->find('section_contacts');

$reviewsRow = array_chunk($database->getReviews(), 4);

$phoneWithoutSpaces= preg_replace('~(\s|\(|\))~','', $generalInfo['phone']);

require_once 'home.php';
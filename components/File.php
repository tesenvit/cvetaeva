<?php

class File
{
    /**
     * Save to directory
     *
     * @param $file
     * @return string
     */
    public static function saveToDir($file) :string
    {
        $resource = pathinfo($file['name']);
        $fileName = mb_strtolower(self::makeHash() . '.' . $resource['extension']);
        move_uploaded_file($file['tmp_name'],'images/' . $fileName);

        return $fileName;
    }

    /**
     * Make hash
     *
     * @return string
     */
    private static function makeHash() :string
    {
        return hash('ripemd160', time() .  rand(10000,99999));
    }

    /**
     * Delete file
     *
     * @param string $fileName
     */
    public static function delete(string $fileName = null) :void
    {
        $path = 'images/' . $fileName;

        if(is_file($path)) unlink($path);
    }

    /**
     * Save file
     *
     * @param array $section
     * @param string $title
     * @return string
     */
    public static function save(array $section, string $title) : ?string
    {
        $resource = $section[$title];
        if(!empty($_FILES[$title]['name']))
        {
            self::delete($section[$title]);
            $resource = self::saveToDir($_FILES[$title]);
        }

        $_POST[$title] = $resource;

        return $resource;
    }
}
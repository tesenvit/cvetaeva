<?php

class Message
{
    public static function store($body, $type = 'success')
    {
        $_SESSION['message'] = $body;
        $_SESSION['messageType'] = $type;
    }

    public static function show()
    {
        $text = '';
        if(isset($_SESSION['message']) && isset($_SESSION['messageType']) && !empty($_SESSION['message']) && !empty($_SESSION['messageType']))
        {
            $text .= '<div class="col-lg-12">
                    <div class="alert alert-' . $_SESSION['messageType'] . ' alert-dismissible fade show" role="alert">
                        <strong>' . $_SESSION['message'] . '</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>';

            unset($_SESSION['message']);
            unset($_SESSION['messageType']);
        }

        return $text;
    }
}
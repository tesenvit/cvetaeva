<?php

class Database
{
    private $host = '127.0.0.1';
    private $name = 'cvetaeva';
    private $user = 'root';
    private $pass = '';
    private $charset = 'utf8';

    private $pdo;

    public function __construct()
    {
        $dsn = "mysql:host=$this->host;dbname=$this->name;charset=$this->charset";

        $opt = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false,
        ];

        try {
            $this->pdo = new PDO($dsn, $this->user, $this->pass, $opt);
        } catch (PDOException $e) {
            die('Mysql error');
        }
    }

    /* Main start */
    public function find($table, $id = 1)
    {
        return $this->pdo->query('SELECT * FROM ' . $table . ' WHERE id =' . $id)->fetch();
    }

    public function update($table, $post, $id = 1)
    {
        $data = [];
        $sql = 'UPDATE ' . $table . ' SET ';

        $count = count($post);
        $i = 1;
        foreach ($post as $key => $value)
        {
            $prefixKey = ':' . $key;
            $comma = $count !== $i ? ',' : '';

            $sql .= $key . '=' . $prefixKey . $comma;

            $data[$prefixKey] = $value ?? '';

            $i++;
        }

        $sql .= ' WHERE id=' . $id;

        return $this->pdo->prepare($sql)->execute($data);
    }
    /* Main end */

    /* Reviews start */
    public function getReviews($type = 'front')
    {
        $where = $type === 'front' ? 'WHERE active = 1 ' : '';
        $sql = 'SELECT * FROM reviews ' . $where . ' ORDER BY id DESC';
        return $this->pdo->query($sql)->fetchAll();
    }

    public function addReview($data)
    {
        $sql = 'INSERT INTO reviews (text, active) VALUES (:text, :active)';
        $stmt = $this->pdo->prepare($sql);

        return $stmt->execute([
            ':text' => $data['text'],
            ':active' => $data['active']
        ]);
    }

    public function findReview($id)
    {
        return $this->pdo->query('SELECT * FROM reviews WHERE id =' . $id)->fetch();
    }

    public function updateReview($id, $data)
    {
        $sql = 'UPDATE reviews SET text = :text, active = :active WHERE id =' . $id;
        return $this->pdo->prepare($sql)->execute([
            ':text' => $data['text'],
            ':active' => $data['active']
        ]);
    }
    public function deleteReview($id)
    {
        $sql = "DELETE FROM reviews WHERE id = :id";
        return $this->pdo->prepare($sql)->execute([
            ':id' => $id
        ]);
    }
    /* Reviews end */
}
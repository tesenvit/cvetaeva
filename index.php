<?php
    header('Content-type: text/html; charset=utf-8');
    session_start();
    date_default_timezone_set('Europe/Kiev');
    error_reporting(E_ALL);

    require_once 'components/Database.php';
    require_once 'components/Message.php';

    $database = new Database();

    $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === 0 ? 'https://' : 'http://';
    define('HOST', $protocol . $_SERVER['HTTP_HOST']);

    if ($_SERVER['REQUEST_URI'] === '/')
    {
        require_once 'front/index.php';
    }
    elseif ($_SERVER['REQUEST_URI'] === '/admin' || preg_match('~^\/admin\/.*$~', $_SERVER['REQUEST_URI']))
    {
        require_once 'back/auth/auth.php';
    }
    else
    {
        header("HTTP/1.0 404 Not Found");
        require_once '404.php';
        exit;
    }
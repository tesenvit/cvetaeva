/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
    config.line_height="1em;1.1em;1.2em;1.3em;1.4em;1.5em;1.6em;1.7em;1.8em;1.9em;" +
        "2em;2.1em;2.2em;2.3em;2.4em;2.5em;2.6em;2.7em;2.8em;2.9em;" +
        "3em;4em;5em;6em;7em;8em;9em;" +
        "10em;20em;30em;";
};

'use strict';

(function ($) {
  "use strict";
  $(document).ready(function () {

    $(window).scroll(function () {
      if ($(this).scrollTop() > 70) {
        $('.header').addClass('fixed-header');
      } else {
        $('.header').removeClass('fixed-header');
      }
    });

    $('.main-slider').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: false,
      prevArrow: '.main-slider-prev',
      nextArrow: '.main-slider-next'
    });

    $('.review-slider').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      arrows: false
    });

    $('.mnu-btn').click(function () {
      $('.header').toggleClass('with-menu');
      $('.main-menu-wrap').slideToggle();
    });

    $('a[href^="#"], *[data-href^="#"]').on('click', function (e) {
      e.preventDefault();
      var t = 1000;
      var d = $(this).attr('data-href') ? $(this).attr('data-href') : $(this).attr('href');
      $('html,body').stop().animate({ scrollTop: $(d).offset().top }, t);
    });

    if (window.matchMedia("(max-width: 1024px)").matches) {
      $(".header-inner").append($(".header-contacts"));
    }
  });

  $('#review-section').find('a').each(function() {
    $(this).wrap('<noindex></noindex>')
  });

})(jQuery);
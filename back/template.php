<!DOCTYPE html>
<html lang="ru">
    <head>
        <title><?= $title ?? 'Cvetaeva.od.ua'?></title>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="/css/admin.css?v=1">
        <link rel="stylesheet" href="/css/fonts.css?v=1">

<!--        <script src="https://cdn.ckeditor.com/4.12.1/full/ckeditor.js"></script>-->
        <script src="/js/ckeditor/ckeditor.js"></script>

    </head>
    <body>
        <div class="container-fluid px-0">
            <div class="d-flex" id="wrapper">
                <!-- Sidebar -->
                <div class="bg-dark border-right border-dark text-white" id="sidebar-wrapper">
                    <div class="sidebar-heading"><a class="text-white" href="http://cvetaeva.od.ua">cvetaeva.od.ua</a></div>
                    <div class="list-group list-group-flus">
                        <a href="/admin" class="list-group-item list-group-item-action bg-dark text-white">Общие настройки</a>
                        <a href="/admin/section-main" class="list-group-item list-group-item-action bg-dark text-white">Секция "Главная"</a>
                        <a href="/admin/section-about-me" class="list-group-item list-group-item-action bg-dark text-white">Секиця "Обо мне"</a>
                        <a href="/admin/section-no-name-1" class="list-group-item list-group-item-action bg-dark text-white">Секция "Без имени 1"</a>
                        <a href="/admin/section-no-name-2" class="list-group-item list-group-item-action bg-dark text-white">Секция "Без имени 2"</a>
                        <a href="/admin/section-video" class="list-group-item list-group-item-action bg-dark text-white">Секция "Видео"</a>
                        <a href="/admin/section-portfolio" class="list-group-item list-group-item-action bg-dark text-white">Секция "Портфолио"</a>
                        <a href="/admin/section-reviews" class="list-group-item list-group-item-action bg-dark text-white">Секция "Отзывы"</a>
                        <a href="/admin/section-contacts" class="list-group-item list-group-item-action bg-dark text-white">Секция "Контакты"</a>
                    </div>
                </div>
                <!-- /#sidebar-wrapper -->

                <!-- Page Content -->
                <div id="page-content-wrapper">

                    <nav class="navbar navbar-expand-lg navbar-dark bg-dark border-bottom">

                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
<!--                                <li class="nav-item active">-->
<!--                                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>-->
<!--                                </li>-->
<!--                                <li class="nav-item">-->
<!--                                    <a class="nav-link" href="#">Link</a>-->
<!--                                </li>-->
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Админ
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="#">Изменить пароль</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="#">Выход</a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </nav>

                    <div class="container-fluid p-4">
                        <div class="row">
                            <?php if(isset($bodyTitle)): ?>
                                <div class="col-lg-12 text-dark">
                                    <h1><?=$bodyTitle?></h1>
                                </div>

                                <div class="col-lg-12">
                                    <hr>
                                </div>
                            <?php endif ?>

                            <?=Message::show()?>

                            <?php if(isset($body)) require_once $body?>
                        </div>
                    </div>
                </div>
                <!-- /#page-content-wrapper -->

            </div>
            <!-- /#wrapper -->
        </div>

        <script src="/js/admin.js"></script>
    </body>
</html>
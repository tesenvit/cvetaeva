<?php
    require_once 'components/File.php';
    define('TEXT_UPDATE_DATA', 'Данные успешно обновлены!');

    switch ($_SERVER['REQUEST_URI'])
    {
        /**
         *  DASHBOARD
         *
         */
        case '/admin':
            $body = 'back/dashboard.php';
            $bodyTitle = 'Общие настройки';

            $generalInfo = $database->find('general_info');

            if(isset($_POST) && !empty($_POST))
            {
                File::save($generalInfo, 'seo_logo');
                File::save($generalInfo, 'logo');

                if($database->update('general_info', $_POST))
                    Message::store(TEXT_UPDATE_DATA);

                $generalInfo = $database->find('general_info');
            }
        break;

        /**
         *  SECTION MAIN
         *
         */
        case '/admin/section-main':
            $body = 'back/sections/main.php';
            $bodyTitle = 'Секция "Главная"';

            $sectionMain = $database->find('section_main');

            if(isset($_POST) && !empty($_POST))
            {
                File::save($sectionMain, 'image');

                if($database->update('section_main', $_POST))
                    Message::store(TEXT_UPDATE_DATA);

                $sectionMain = $database->find('section_main');
            }
        break;

        /**
         *  SECTION ABOUT ME
         *
         */
        case '/admin/section-about-me':
            $body = 'back/sections/about_me.php';
            $bodyTitle = 'Секция "Обо мне"';

            $sectionAboutMe = $database->find('section_about_me');

            if(isset($_POST) && !empty($_POST))
            {
                File::save($sectionAboutMe, 'image');
                File::save($sectionAboutMe, 'image_desc');

                if($database->update('section_about_me', $_POST))
                    Message::store(TEXT_UPDATE_DATA);

                $sectionAboutMe = $database->find('section_about_me');
            }
        break;

        /**
         *  SECTION NONAME 1
         *
         */
        case '/admin/section-no-name-1':
            $body = 'back/sections/noname_1.php';
            $bodyTitle = 'Секция "Без имени 1"';

            $sectionNoname1 = $database->find('section_noname_1');

            if(isset($_POST) && !empty($_POST))
            {
                File::save($sectionNoname1, 'image');
                File::save($sectionNoname1, 'image_desc');

                if($database->update('section_noname_1', $_POST))
                    Message::store(TEXT_UPDATE_DATA);

                $sectionNoname1 = $database->find('section_noname_1');
            }
        break;
        /**
         *  SECTION NONAME 2
         *
         */
        case '/admin/section-no-name-2':
            $body = 'back/sections/noname_2.php';
            $bodyTitle = 'Секция "Без имени 2"';

            $sectionNoname2 = $database->find('section_noname_2');

            if(isset($_POST) && !empty($_POST))
            {
                File::save($sectionNoname2, 'image');
                File::save($sectionNoname2, 'image_desc');

                if($database->update('section_noname_2', $_POST))
                    Message::store(TEXT_UPDATE_DATA);

                $sectionNoname2 = $database->find('section_noname_2');
            }
        break;

        /**
         *  SECTION VIDEO
         *
         */
        case '/admin/section-video':
            $body = 'back/sections/video.php';
            $bodyTitle = 'Секция "Видео"';

            $sectionVideo = $database->find('section_video');

            if(isset($_POST) && !empty($_POST))
            {
                File::save($sectionVideo, 'poster');
                File::save($sectionVideo, 'video');

                if($database->update('section_video', $_POST))
                    Message::store(TEXT_UPDATE_DATA);

                $sectionVideo = $database->find('section_video');
            }
        break;

        /**
         *  SECTION PORTFOLIO
         *
         */
        case '/admin/section-portfolio':
            $body = 'back/sections/portfolio.php';
            $bodyTitle = 'Секция "Портфолио"';

            $sectionPortfolio = $database->find('section_portfolio');

            if(isset($_POST) && !empty($_POST))
            {
                File::save($sectionPortfolio, 'image_1');
                File::save($sectionPortfolio, 'image_2');
                File::save($sectionPortfolio, 'image_3');
                File::save($sectionPortfolio, 'image_4');
                File::save($sectionPortfolio, 'image_5');
                File::save($sectionPortfolio, 'image_6');

                if($database->update('section_portfolio', $_POST))
                    Message::store(TEXT_UPDATE_DATA);

                $sectionPortfolio = $database->find('section_portfolio');
            }
        break;

        /**
         *  SECTION REVIEWS
         *
         */
        // Show all
        case '/admin/section-reviews':
            $body = 'back/sections/reviews/main.php';
            $bodyTitle = 'Секция "Отзывы"';

            $sectionReviews = $database->find('section_reviews');
            $reviews = $database->getReviews('admin');

            if(isset($_POST) && !empty($_POST))
            {
                File::save($sectionReviews, 'image');

                if($database->update('section_reviews', $_POST))
                    Message::store(TEXT_UPDATE_DATA);

                $sectionReviews = $database->find('section_reviews');
            }
        break;

        // Add new review
        case '/admin/section-reviews/add':
            $body = 'back/sections/reviews/add.php';
            $bodyTitle = 'Добавить отзыв';

            if(isset($_POST) && !empty($_POST))
            {
                if($database->addReview($_POST))
                {
                    Message::store('Отзыв успешно добавлен!');
                    header('Location: /admin/section-reviews');
                    die;
                }
            }
        break;

        // Update the review
        case preg_match('~^\/admin\/section-reviews\/update\/\d{1,}$~', $_SERVER['REQUEST_URI']) ? true : false :
            $body = 'back/sections/reviews/update.php';
            $bodyTitle = 'Редактировать отзыв';

            $explodedUri = explode('/', $_SERVER['REQUEST_URI']);
            $id = end($explodedUri);

            $review = $database->findReview($id);

            if(isset($_POST) && !empty($_POST))
            {
                if($database->updateReview($id, $_POST))
                    Message::store(TEXT_UPDATE_DATA);

                $review = $database->findReview($id);
            }
        break;

        // Delete the review
        case preg_match('~^\/admin\/section-reviews\/delete\/\d{1,}$~', $_SERVER['REQUEST_URI']) ? true : false :
            $explodedUri = explode('/', $_SERVER['REQUEST_URI']);
            $id = end($explodedUri);

            if($database->deleteReview($id))
            {
                Message::store('Отзыв успешно удален!','danger');
                header('Location: /admin/section-reviews');
                die;
            }
        break;

        /**
         *  SECTION CONTACTS
         *
         */
        case  '/admin/section-contacts':
            $body = 'back/sections/contacts.php';

            $bodyTitle = 'Секция "Контакты"';

            $sectionContacts = $database->find('section_contacts');

            if(isset($_POST) && !empty($_POST))
            {
                File::save($sectionContacts, 'image');

                if($database->update('section_contacts', $_POST))
                    Message::store(TEXT_UPDATE_DATA);

                $sectionContacts = $database->find('section_contacts');
            }
        break;

        /**
         *  404
         *
         */
        default:
            require_once '404.php'; die;
    }

    unset($_POST);
    require_once 'template.php';

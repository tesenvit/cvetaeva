<div class="col-lg-12">
    <form method="POST" enctype='multipart/form-data'>
        <div class="form-group">
            <label for="poster">Постер</label>
            <input type="file" class="form-control-file" id="poster" name="poster">
            <? if(isset($sectionVideo['poster']) && !empty($sectionVideo['poster'])):?>
                <br>
                <img src="/images/<?=$sectionVideo['poster']?>" alt="seo logo" width="300" height="200">
            <? endif ?>
            <small class="form-text text-muted"><span class="text-danger">*</span>&nbsp; 940x530px</small>
            <hr>
        </div>

        <div class="form-group">
            <label for="image">Видео</label>
            <input type="file" class="form-control-file" id="video" name="video">
            <? if(isset($sectionVideo['video']) && !empty($sectionVideo['video'])):?>
                <br>
                <video controls="controls" width="300" height="200">
                    <source src="/images/<?=$sectionVideo['video']?>">
                </video>
            <? endif ?>
            <small class="form-text text-muted"><span class="text-danger">*</span>&nbsp;max 100 MB</small>
            <hr>
        </div>

        <div class="form-group">
            <label for="text-value">Описание</label>
            <textarea class="form-control" id="text-value" rows="3" name="description"><?=$sectionVideo['description']?></textarea>
        </div>

        <div class="form-group mt-4">
            <button type="submit" class="btn pink-background text-light">Сохранить</button>
        </div>
    </form>
</div>
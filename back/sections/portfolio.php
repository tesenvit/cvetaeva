<div class="col-lg-12">
    <form method="POST" enctype='multipart/form-data'>
        <div class="form-group">
            <label for="image_1">Картинка 1</label>
            <input type="file" class="form-control-file" id="image_1" name="image_1">
            <? if(isset($sectionPortfolio['image_1']) && !empty($sectionPortfolio['image_1'])):?>
                <br>
                <img src="/images/<?=$sectionPortfolio['image_1']?>" alt="seo logo" width="400" height="200">
            <? endif ?>
            <small class="form-text text-muted"><span class="text-danger">*</span>&nbsp;960x480px</small>
            <hr>
        </div>

        <div class="form-group">
            <label for="image_2">Картинка 2</label>
            <input type="file" class="form-control-file" id="image_2" name="image_2">
            <? if(isset($sectionPortfolio['image_2']) && !empty($sectionPortfolio['image_2'])):?>
                <br>
                <img src="/images/<?=$sectionPortfolio['image_2']?>" alt="seo logo" width="200" height="400">
            <? endif ?>
            <small class="form-text text-muted"><span class="text-danger">*</span>&nbsp;480x960px</small>
            <hr>
        </div>

        <div class="form-group">
            <label for="image_3">Картинка 3</label>
            <input type="file" class="form-control-file" id="image_3" name="image_3">
            <? if(isset($sectionPortfolio['image_3']) && !empty($sectionPortfolio['image_3'])):?>
                <br>
                <img src="/images/<?=$sectionPortfolio['image_3']?>" alt="seo logo" width="300" height="300">
            <? endif ?>
            <small class="form-text text-muted"><span class="text-danger">*</span>&nbsp;480x480px</small>
            <hr>
        </div>

        <div class="form-group">
            <label for="image_4">Картинка 4</label>
            <input type="file" class="form-control-file" id="image_4" name="image_4">
            <? if(isset($sectionPortfolio['image_4']) && !empty($sectionPortfolio['image_4'])):?>
                <br>
                <img src="/images/<?=$sectionPortfolio['image_4']?>" alt="seo logo" width="300" height="300">
            <? endif ?>
            <small class="form-text text-muted"><span class="text-danger">*</span>&nbsp;480x480px</small>
            <hr>
        </div>

        <div class="form-group">
            <label for="image_5">Картинка 5</label>
            <input type="file" class="form-control-file" id="image_5" name="image_5">
            <? if(isset($sectionPortfolio['image_5']) && !empty($sectionPortfolio['image_5'])):?>
                <br>
                <img src="/images/<?=$sectionPortfolio['image_5']?>" alt="seo logo" width="300" height="300">
            <? endif ?>
            <small class="form-text text-muted"><span class="text-danger">*</span>&nbsp;480x480px</small>
            <hr>
        </div>

        <div class="form-group">
            <label for="image_6">Картинка 6</label>
            <input type="file" class="form-control-file" id="image_6" name="image_6">
            <? if(isset($sectionPortfolio['image_6']) && !empty($sectionPortfolio['image_6'])):?>
                <br>
                <img src="/images/<?=$sectionPortfolio['image_6']?>" alt="seo logo" width="300" height="300">
            <? endif ?>
            <small class="form-text text-muted"><span class="text-danger">*</span>&nbsp;480x480px</small>
            <hr>
        </div>

        <div class="form-group">
            <label for="text-value">Описание</label>
            <textarea class="form-control" id="text-value" rows="3" name="description"><?=$sectionPortfolio['description']?></textarea>
        </div>

        <div class="form-group mt-4">
            <button type="submit" class="btn pink-background text-light">Сохранить</button>
        </div>
    </form>
</div>
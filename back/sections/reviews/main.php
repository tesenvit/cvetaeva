<div class="col-lg-12">
    <form method="POST" enctype='multipart/form-data'>
        <div class="form-group">
            <label for="image">Картинка</label>
            <input type="file" class="form-control-file" id="image" name="image">
            <? if(isset($sectionReviews['image']) && !empty($sectionReviews['image'])):?>
                <br>
                <img src="/images/<?=$sectionReviews['image']?>" alt="seo logo" width="300" height="200">
            <? endif ?>
            <small class="form-text text-muted"><span class="text-danger">*</span>&nbsp; 1900x2180px</small>
            <hr>
        </div>

        <div class="form-group">
            <label for="text-value">Описание</label>
            <textarea class="form-control" id="text-value" rows="3" name="description"><?=$sectionReviews['description']?></textarea>
        </div>

        <div class="form-group mt-4">
            <button type="submit" class="btn pink-background text-light">Сохранить</button>
        </div>
    </form>
</div>

<div class="col-lg-12 separator"></div>

<div class="col-lg-12">
    <h2>Отзывы</h2>
    <hr>
    <a href="/admin/section-reviews/add" class="btn btn-primary">Добавить</a>
    <hr>
    <br>
    <?php if($reviews):?>
    <table class="table table-bordered">
        <thead>
            <tr class="text-lg-center">
                <th scope="col">ID</th>
                <th scope="col">Описание</th>
                <th scope="col">Статус</th>
                <th scope="col"></th>
                <th scope="col"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($reviews as $review):?>
                <tr class="text-lg-center <?=!$review['active'] ? 'bg-light' : '' ?>">
                    <td><?=$review['id']?></td>
                    <td><?=mb_substr(strip_tags($review['text']),0,70)?>...</td>
                    <td class="<?=$review['active'] ? 'text-success' : 'text-danger'?>"><?=$review['active'] ? 'Активно' : 'Не активно'?></td>
                    <td><a href="/admin/section-reviews/update/<?=$review['id']?>" class="btn btn-info">Подробнее</a></td>
                    <td><a href="/admin/section-reviews/delete/<?=$review['id']?>" class="btn btn-danger" onclick="return confirm('Вы действительно хотите удалить отзыв?')">Удалить</a></td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
    <?php endif ?>
</div>
<div class="col-lg-12">
    <form method="POST" enctype='multipart/form-data'>
        <div class="form-group">
            <label for="text-value">Описание</label>
            <textarea class="form-control" id="text-value" rows="3" name="text"></textarea>
        </div>

        <div class="custom-control custom-radio">
            <input type="radio" class="custom-control-input" id="active"  name="active" value="1" checked>
            <label class="custom-control-label" for="active">Активно</label>
        </div>

        <div class="custom-control custom-radio">
            <input type="radio" class="custom-control-input" id="not_active" name="active" value="0">
            <label class="custom-control-label" for="not_active">Не активно</label>
        </div>

        <div class="form-group mt-4">
            <button type="submit" class="btn pink-background text-light">Сохранить</button>
        </div>
    </form>
</div>
<div class="col-lg-12">

    <div class="row">
        <div class="col-lg-6">
            <h4>ID: <?=$review['id']?></h4>
        </div>
        <div class="col-lg-6 text-right">
            <a href="/admin/section-reviews/delete/<?=$review['id']?>" class="btn btn-danger" onclick="return confirm('Вы действительно хотите удалить отзыв?')">Удалить</a>
        </div>
    </div>

    <br>
    <form method="POST" enctype='multipart/form-data'>
        <div class="form-group">
            <label for="text-value">Текст</label>
            <textarea class="form-control" id="text-value" rows="3" name="text"><?=$review['text']?></textarea>
        </div>

        <div class="custom-control custom-radio">
            <input type="radio" class="custom-control-input" id="active"  name="active" value="1" <?=$review['active'] ? 'checked' : ''?>>
            <label class="custom-control-label" for="active">Активно</label>
        </div>

        <div class="custom-control custom-radio">
            <input type="radio" class="custom-control-input" id="not_active" name="active" value="0" <?=!$review['active'] ? 'checked' : ''?>>
            <label class="custom-control-label" for="not_active">Не активно</label>
        </div>

        <hr>

        <div class="form-group mt-4">
            <button type="submit" class="btn pink-background text-light">Обновить</button>
        </div>
    </form>
</div>

<div class="col-lg-12">
    <form method="POST" enctype='multipart/form-data'>
        <div class="form-group">
            <label for="image">Картинка</label>
            <input type="file" class="form-control-file" id="image" name="image">
            <? if(isset($sectionContacts['image']) && !empty($sectionContacts['image'])):?>
                <br>
                <img src="/images/<?=$sectionContacts['image']?>" alt="seo logo" width="400" height="200">
            <? endif ?>
            <small class="form-text text-muted"><span class="text-danger">*</span>&nbsp; 1920x744px</small>
            <hr>
        </div>

        <div class="form-group">
            <label for="text-value">Текст</label>
            <textarea class="form-control" id="text-value" rows="3" name="text"><?=$sectionContacts['text']?></textarea>
        </div>

        <div class="form-group mt-4">
            <button type="submit" class="btn pink-background text-light">Сохранить</button>
        </div>
    </form>
</div>
<div class="col-lg-12">
    <form method="POST" enctype='multipart/form-data'>
        <div class="form-group">
            <label for="image">Картинка</label>
            <input type="file" class="form-control-file" id="image" name="image">
            <? if(isset($sectionNoname2['image']) && !empty($sectionNoname2['image'])):?>
                <br>
                <img src="/images/<?=$sectionNoname2['image']?>" alt="seo logo" width="300" height="200">
            <? endif ?>
            <small class="form-text text-muted"><span class="text-danger">*</span>&nbsp; 960x610px</small>
            <hr>
        </div>

        <div class="form-group">
            <label for="image">Картинка под текст</label>
            <input type="file" class="form-control-file" id="image_desc" name="image_desc">
            <? if(isset($sectionNoname2['image_desc']) && !empty($sectionNoname2['image_desc'])):?>
                <br>
                <img src="/images/<?=$sectionNoname2['image_desc']?>" alt="seo logo" width="300" height="200">
            <? endif ?>
            <small class="form-text text-muted"><span class="text-danger">*</span>&nbsp; 960x610px</small>
            <hr>
        </div>

        <div class="form-group">
            <label for="text-value">Текст</label>
            <textarea class="form-control" id="text-value" rows="3" name="text"><?=$sectionNoname2['text']?></textarea>
        </div>

        <div class="form-group mt-4">
            <button type="submit" class="btn pink-background text-light">Сохранить</button>
        </div>
    </form>
</div>
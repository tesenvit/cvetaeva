<div class="col-lg-12">
    <form method="POST" enctype='multipart/form-data'>
        <div class="form-group">
            <label for="title">SEO Title</label>
            <input type="text" class="form-control" id="title" placeholder="SEO title" name="title" value="<?=$generalInfo['title'] ?? ''?>">
        </div>

        <div class="form-group">
            <label for="description">SEO description</label>
            <input type="text" class="form-control" id="description" placeholder="SEO description" name="description" value="<?=$generalInfo['description'] ?? ''?>">
        </div>

        <div class="form-group">
            <label for="phone">Телефон</label>
            <input type="text" class="form-control" id="phone" placeholder="Телефон" name="phone" value="<?=$generalInfo['phone'] ?? ''?>">
        </div>

        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" placeholder="Email" name="email" value="<?=$generalInfo['email'] ?? ''?>">
        </div>

        <div class="form-group">
            <label for="insta">Ссылка Instagram</label>
            <input type="text" class="form-control" id="insta" placeholder="Ссылка Instagram" name="link_insta" value="<?=$generalInfo['link_insta'] ?? ''?>">
        </div>

        <div class="form-group">
            <label for="facebook">Ссылка Facebook</label>
            <input type="text" class="form-control" id="facebook" placeholder="Ссылка Facebook" name="link_facebook" value="<?=$generalInfo['link_facebook'] ?? ''?>">
        </div>

        <div class="form-group">
            <label for="youtube">Ссылка Youtube</label>
            <input type="text" class="form-control" id="youtube" placeholder="Ссылка Youtube" name="link_youtube" value="<?=$generalInfo['link_youtube'] ?? ''?>">
        </div>

        <div class="form-group">
            <label for="seo_logo">SEO Логотип</label>
            <input type="file" class="form-control-file" id="seo_logo" name="seo_logo">
            <? if(isset($generalInfo['seo_logo']) && !empty($generalInfo['seo_logo'])):?>
                <br>
                <img src="/images/<?=$generalInfo['seo_logo']?>" alt="seo logo" width="200" height="200">
            <? endif ?>
            <small class="form-text text-muted"><span class="text-danger">*</span>&nbsp;min - 200x200px; max - 600x600px</small>
            <hr>
        </div>

        <div class="form-group">
            <label for="logo">Логотип</label>
            <input type="file" class="form-control-file" id="logo" name="logo">
            <? if(isset($generalInfo['logo']) && !empty($generalInfo['logo'])):?>
                <br>
                <img src="/images/<?=$generalInfo['logo']?>" alt="seo logo" width="200" height="200">
            <? endif ?>
            <hr>
        </div>

        <div class="form-group mt-4">
            <button type="submit" class="btn pink-background text-light">Сохранить</button>
        </div>
    </form>
</div>
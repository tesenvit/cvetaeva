<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Login</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="/css/admin.css">
    <link rel="stylesheet" href="/css/fonts.css">
</head>
<body>
<div class="container">
    <div class="login-form mt-5 pt-5">
        <form method="POST">
            <div class="form-row">
                <div class="form-group col-lg-4 offset-lg-4">
                    <label for="email"></label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
                </div>

                <div class="form-group col-lg-4 offset-lg-4">
                    <label for="password"></label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Пароль" required>
                </div>

                <div class="form-group col-lg-4 offset-lg-4 mt-3">
                    <button type="submit" class="btn pink-background text-light">Вход</button>
                </div>

                <? if(isset($loginError) && $loginError):?>
                    <div class="form-group col-lg-4 offset-lg-4 mt-3">
                        <strong class="text-danger">Не правильный Email или пароль</strong>
                    </div>
                <? endif ?>
            </div>
        </form>
    </div>
</div>

</body>
</html>





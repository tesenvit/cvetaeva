<?php
if (isset($_SESSION['adminCheck']) && $_SESSION['adminCheck'] === 1)
{
    $title = 'Admin panel';
    require_once 'back/index.php';
}
else
{
    if($_POST)
    {
        $loginError = true;

        if((isset($_POST['email']) && !empty($_POST['email'])) && (isset($_POST['password']) && !empty($_POST['password'])))
        {
            $admin = $database->find('admins');

            // password_hash("password", PASSWORD_BCRYPT, ['cost'=>12]);
            if($admin['email'] === $_POST['email'] && password_verify($_POST['password'], $admin['password']))
            {
                $loginError = false;
                $_SESSION['adminCheck'] = 1;
                $title = 'Admin';
                unset($_POST);
                require_once 'back/index.php';
                die;
            }
        }
    }

    require_once 'back/auth/login_form.php';
}






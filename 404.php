<!DOCTYPE html>
<html lang="ru">

<head>
    <title>404</title>
    <!-- styles -->
    <link rel="stylesheet" href="<?=HOST?>/css/fonts.css">


    <style>
        .body-404-page {
            background: #222;
        }
        .h1-404, .page-not-found-text {
            font-weight: 900;
            color: #eebef1;
            font-family: HelveticaNeueCyr;
            line-height: 1em;
            width: 99%;
            margin: 0;
            position: absolute;
            text-align: center;
            /*text-shadow: 0 1px 0 #fc9ce9, 0 2px 0 #fb92e7, 0 3px 0 #fb88e5, 0 4px 0 #fb7ee3, 0 5px 0 #fa74e1, 0 6px 0 #fa6adf, 0 7px 0 #fa60dd, 0 8px 0 #f956da, 0 0 5px rgba(255, 142, 234, .05), 0 -1px 3px rgba(255, 142, 234, .2), 0 9px 9px rgba(255, 142, 234, .5), 0 12px 12px rgba(255, 142, 234, .5), 0 15px 15px rgba(255, 142, 234, .5);*/
            text-shadow: 0 1px 0 #0e0e0e, 0 2px 0 #090909, 0 3px 0 #030303, 0 4px 0 #000, 0 5px 0 #000, 0 6px 0 #000, 0 7px 0 #000, 0 8px 0 #000, 0 0 5px rgba(10, 7, 7, .05), 0 -1px 3px rgba(10, 7, 7, .2), 0 9px 9px rgba(10, 7, 7, .5), 0 12px 12px rgba(10, 7, 7, .5), 0 15px 15px rgba(10, 7, 7, .5);
        }
        .h1-404{
            font-size: 200px;
            top: 40%;
            transform: translateY(-80%);
        }
        .page-not-found-text {
            font-size: 50px;
            top: 50%;
            transform: translateY(-50%);
        }
    </style>

</head>

<body class="body-404-page">
    <h1 class="h1-404">404</h1>
    <p class="page-not-found-text">СТРАНИЦА НЕ НАЙДЕНА</p>
</body>

</html>